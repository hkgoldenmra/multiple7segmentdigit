#include "Multiple7SegmentDigit.h"

const byte Multiple7SegmentDigit::LED_SEGMENTS[8] = {
	Multiple7SegmentDigit::LED_MIDDLE,
	Multiple7SegmentDigit::LED_TOP_LEFT,
	Multiple7SegmentDigit::LED_TOP,
	Multiple7SegmentDigit::LED_TOP_RIGHT,
	Multiple7SegmentDigit::LED_DOT,
	Multiple7SegmentDigit::LED_BOTTOM_RIGHT,
	Multiple7SegmentDigit::LED_BOTTOM,
	Multiple7SegmentDigit::LED_BOTTOM_LEFT,
};

const byte Multiple7SegmentDigit::LED_DIGITS[16] = {
	Multiple7SegmentDigit::LED_TOP_LEFT | Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_TOP_RIGHT | Multiple7SegmentDigit::LED_BOTTOM_RIGHT | Multiple7SegmentDigit::LED_BOTTOM | Multiple7SegmentDigit::LED_BOTTOM_LEFT,
	Multiple7SegmentDigit::LED_TOP_RIGHT | Multiple7SegmentDigit::LED_BOTTOM_RIGHT,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_TOP_RIGHT | Multiple7SegmentDigit::LED_BOTTOM | Multiple7SegmentDigit::LED_BOTTOM_LEFT,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_TOP_RIGHT | Multiple7SegmentDigit::LED_BOTTOM_RIGHT | Multiple7SegmentDigit::LED_BOTTOM,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP_LEFT | Multiple7SegmentDigit::LED_TOP_RIGHT | Multiple7SegmentDigit::LED_BOTTOM_RIGHT,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP_LEFT | Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_BOTTOM_RIGHT | Multiple7SegmentDigit::LED_BOTTOM,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP_LEFT | Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_BOTTOM_RIGHT | Multiple7SegmentDigit::LED_BOTTOM | Multiple7SegmentDigit::LED_BOTTOM_LEFT,
	Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_TOP_RIGHT | Multiple7SegmentDigit::LED_BOTTOM_RIGHT,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP_LEFT | Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_TOP_RIGHT | Multiple7SegmentDigit::LED_BOTTOM_RIGHT | Multiple7SegmentDigit::LED_BOTTOM | Multiple7SegmentDigit::LED_BOTTOM_LEFT,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP_LEFT | Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_TOP_RIGHT | Multiple7SegmentDigit::LED_BOTTOM | Multiple7SegmentDigit::LED_BOTTOM_RIGHT,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP_LEFT | Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_TOP_RIGHT | Multiple7SegmentDigit::LED_BOTTOM_RIGHT | Multiple7SegmentDigit::LED_BOTTOM_LEFT,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP_LEFT | Multiple7SegmentDigit::LED_BOTTOM_RIGHT | Multiple7SegmentDigit::LED_BOTTOM | Multiple7SegmentDigit::LED_BOTTOM_LEFT,
	Multiple7SegmentDigit::LED_TOP_LEFT | Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_BOTTOM | Multiple7SegmentDigit::LED_BOTTOM_LEFT,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP_RIGHT | Multiple7SegmentDigit::LED_BOTTOM_RIGHT | Multiple7SegmentDigit::LED_BOTTOM | Multiple7SegmentDigit::LED_BOTTOM_LEFT,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP_LEFT | Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_BOTTOM | Multiple7SegmentDigit::LED_BOTTOM_LEFT,
	Multiple7SegmentDigit::LED_MIDDLE | Multiple7SegmentDigit::LED_TOP_LEFT | Multiple7SegmentDigit::LED_TOP | Multiple7SegmentDigit::LED_BOTTOM_LEFT
};

Multiple7SegmentDigit::Multiple7SegmentDigit(byte middlePin, byte topLeftPin, byte topPin, byte topRightPin, byte dotPin, byte bottomRightPin, byte bottomPin, byte bottomLeftPin, byte powerPins[]) {
	this->segmentPins[0] = middlePin;
	this->segmentPins[1] = topLeftPin;
	this->segmentPins[2] = topPin;
	this->segmentPins[3] = topRightPin;
	this->segmentPins[4] = dotPin;
	this->segmentPins[5] = bottomRightPin;
	this->segmentPins[6] = bottomPin;
	this->segmentPins[7] = bottomLeftPin;
	const byte LENGTH = sizeof(this->powerPins) / sizeof(byte);
	for (byte i = 0; i < LENGTH; i++) {
		this->powerPins[i] = powerPins[i];
	}
}

void Multiple7SegmentDigit::initial() {
	for (byte segmentPin : this->segmentPins) {
		pinMode(segmentPin, INPUT);
		digitalWrite(segmentPin, HIGH);
	}
	for (byte powerPin : this->powerPins) {
		pinMode(powerPin, OUTPUT);
		digitalWrite(powerPin, HIGH);
	}
}

void Multiple7SegmentDigit::setDigit(byte value, bool enabled) {
	const byte LENGTH = sizeof(Multiple7SegmentDigit::LED_SEGMENTS) / sizeof(byte);
	for (byte i = 0; i < LENGTH; i++) {
		if((value & Multiple7SegmentDigit::LED_SEGMENTS[i]) > 0) {
			digitalWrite(this->segmentPins[i], (enabled ? LOW : HIGH));
		}
	}
}