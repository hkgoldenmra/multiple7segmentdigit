#include "Multiple7SegmentDigit.h"

// Pin 9 = middlePin
// Pin 8 = topLeftPin
// Pin 7 = topPin
// Pin 6 = topRightPin
// Pin 2 = dotPin
// Pin 3 = bottomRightPin
// Pin 4 = bottomPin
// Pin 5 = bottomLeftPin
// Pin 10 = powerPins
byte powerPins[1] = {10};
Multiple7SegmentDigit m7sd(9, 8, 7, 6, 2, 3, 4, 5, powerPins);
const byte STEPS = 8;
byte x = 0;

void setup() {
	m7sd.initial();
}

void loop() {
	x %= 0x10;
	m7sd.setDigit(Multiple7SegmentDigit::LED_DIGITS[8] | Multiple7SegmentDigit::LED_DOT, false);
	m7sd.setDigit(Multiple7SegmentDigit::LED_DIGITS[x], true);
	for (byte i = 0; i < STEPS; i++) {
		m7sd.setDigit(Multiple7SegmentDigit::LED_DOT, i % 2 == 0);
		delay(1000 / STEPS);
	}
	x++;
}