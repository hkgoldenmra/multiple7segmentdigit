#include <Arduino.h>

class Multiple7SegmentDigit {
	private:
		byte segmentPins[8];
		byte powerPins[255];
	public:
		static const byte LED_MIDDLE = 0x01;
		static const byte LED_TOP_LEFT = 0x02;
		static const byte LED_TOP = 0x04;
		static const byte LED_TOP_RIGHT = 0x08;
		static const byte LED_DOT = 0x10;
		static const byte LED_BOTTOM_RIGHT = 0x20;
		static const byte LED_BOTTOM = 0x40;
		static const byte LED_BOTTOM_LEFT = 0x80;
		static const byte LED_SEGMENTS[8];
		static const byte LED_DIGITS[16];
		Multiple7SegmentDigit(byte, byte, byte, byte, byte, byte, byte, byte, byte[]);
		void initial();
		void setDigit(byte, bool);
};